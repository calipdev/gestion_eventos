﻿
CREATE SEQUENCE evento_idevento_seq;

CREATE TABLE evento (
                idEvento INTEGER NOT NULL DEFAULT nextval('evento_idevento_seq'),
                nombre VARCHAR NOT NULL,
                tipo VARCHAR NOT NULL,
                fechaInicio DATE NOT NULL,
                fechaFin DATE NOT NULL,
                costo REAL NOT NULL,
                tipoVenta VARCHAR NOT NULL,
                direccion VARCHAR NOT NULL,
                ciudad VARCHAR NOT NULL,
                pais VARCHAR NOT NULL,
                CONSTRAINT evento_pk PRIMARY KEY (idEvento)
);


ALTER SEQUENCE evento_idevento_seq OWNED BY evento.idEvento;

CREATE SEQUENCE paquete_idpaquete_seq;

CREATE TABLE paquete (
                idPaquete INTEGER NOT NULL DEFAULT nextval('paquete_idpaquete_seq'),
                nombre VARCHAR NOT NULL,
                descripcion VARCHAR NOT NULL,
                idEvento INTEGER NOT NULL,
                CONSTRAINT paquete_pk PRIMARY KEY (idPaquete)
);


ALTER SEQUENCE paquete_idpaquete_seq OWNED BY paquete.idPaquete;

CREATE SEQUENCE descuento_iddescuento_seq;

CREATE TABLE descuento (
                idDescuento INTEGER NOT NULL DEFAULT nextval('descuento_iddescuento_seq'),
                monto REAL NOT NULL,
                descripcion VARCHAR NOT NULL,
                porcentage INTEGER NOT NULL,
                idPaquete INTEGER NOT NULL,
                CONSTRAINT descuento_pk PRIMARY KEY (idDescuento)
);


ALTER SEQUENCE descuento_iddescuento_seq OWNED BY descuento.idDescuento;

CREATE SEQUENCE cuota_idcuota_seq;

CREATE TABLE cuota (
                idCuota INTEGER NOT NULL DEFAULT nextval('cuota_idcuota_seq'),
                costo REAL NOT NULL,
                ocupacion VARCHAR NOT NULL,
                idPaquete INTEGER NOT NULL,
                CONSTRAINT cuota_pk PRIMARY KEY (idCuota),
                CONSTRAINT paquete_cuota_fk FOREIGN KEY (idpaquete)
                   REFERENCES paquete (idpaquete) MATCH SIMPLE
                   ON UPDATE NO ACTION ON DELETE NO ACTION
);


ALTER SEQUENCE cuota_idcuota_seq OWNED BY cuota.idCuota;


CREATE SEQUENCE actividad_idactividad_seq;

CREATE TABLE actividad (
                idActividad INTEGER NOT NULL DEFAULT nextval('actividad_idactividad_seq'),
                nombre VARCHAR NOT NULL,
                ciudad VARCHAR NOT NULL,
                lugar VARCHAR NOT NULL,
                hora VARCHAR NOT NULL,
                duracion VARCHAR NOT NULL,
                descripcion VARCHAR NOT NULL,
                maxParticipantes INTEGER NOT NULL,
                ocupada BOOLEAN NOT NULL,
                idEvento INTEGER NOT NULL,
                CONSTRAINT actividad_pk PRIMARY KEY (idActividad)
);


ALTER SEQUENCE actividad_idactividad_seq OWNED BY actividad.idActividad;

CREATE SEQUENCE tematica_idtematica_seq;

CREATE TABLE tematica (
                idTematica INTEGER NOT NULL DEFAULT nextval('tematica_idtematica_seq'),
                nombre VARCHAR NOT NULL,
                descripcion VARCHAR NOT NULL,
                idActividad INTEGER NOT NULL,
                CONSTRAINT tematica_pk PRIMARY KEY (idTematica)
);


ALTER SEQUENCE tematica_idtematica_seq OWNED BY tematica.idTematica;

CREATE SEQUENCE hotel_idhotel_seq;

CREATE TABLE hotel (
                idHotel INTEGER NOT NULL DEFAULT nextval('hotel_idhotel_seq'),
                nombre VARCHAR NOT NULL,
                categoria VARCHAR NOT NULL,
                ciudad VARCHAR NOT NULL,
                direccion VARCHAR NOT NULL,
                telefono VARCHAR NOT NULL,
                descripcion VARCHAR NOT NULL,
                idEvento INTEGER NOT NULL,
                CONSTRAINT hotel_pk PRIMARY KEY (idHotel)
);


ALTER SEQUENCE hotel_idhotel_seq OWNED BY hotel.idHotel;

CREATE SEQUENCE habitacion_idhabitacion_seq;

CREATE TABLE habitacion (
                idHabitacion INTEGER NOT NULL DEFAULT nextval('habitacion_idhabitacion_seq'),
                numero VARCHAR NOT NULL,
                tipo VARCHAR NOT NULL,
                costo REAL NOT NULL,
                maxPersonas INTEGER NOT NULL,
                ocupada BOOLEAN NOT NULL,
                idHotel INTEGER NOT NULL,
                CONSTRAINT habitacion_pk PRIMARY KEY (idHabitacion)
);


ALTER SEQUENCE habitacion_idhabitacion_seq OWNED BY habitacion.idHabitacion;

CREATE SEQUENCE cuenta_idcuenta_seq;

CREATE TABLE cuenta (
                idCuenta INTEGER NOT NULL DEFAULT nextval('cuenta_idcuenta_seq'),
                username VARCHAR(20) NOT NULL,
                password VARCHAR NOT NULL,
                rol VARCHAR,
                fechaAlta DATE,
                CONSTRAINT cuenta_pk PRIMARY KEY (idCuenta)
);


ALTER SEQUENCE cuenta_idcuenta_seq OWNED BY cuenta.idCuenta;

CREATE SEQUENCE usuario_idusuario_seq;

CREATE TABLE usuario (
                idUsuario INTEGER NOT NULL DEFAULT nextval('usuario_idusuario_seq'),
                nombre VARCHAR(40) NOT NULL,
                apellido1 VARCHAR(40) NOT NULL,
                apellido2 VARCHAR(40) NOT NULL,
                email VARCHAR NOT NULL,
                direccion VARCHAR NOT NULL,
                ciudad VARCHAR NOT NULL,
                pais VARCHAR NOT NULL,
                codigoPostal VARCHAR NOT NULL,
                telefono VARCHAR NOT NULL,
                compania VARCHAR NOT NULL,
                idCuenta INTEGER NOT NULL,
                CONSTRAINT usuario_pk PRIMARY KEY (idUsuario)
);


ALTER SEQUENCE usuario_idusuario_seq OWNED BY usuario.idUsuario;

CREATE SEQUENCE reservacion_idreservacion_seq;

CREATE TABLE reservacion (
                idReservacion INTEGER NOT NULL DEFAULT nextval('reservacion_idreservacion_seq'),
                idUsuario INTEGER NOT NULL,
                idHabitacion INTEGER NOT NULL,
                fechaEntrada DATE NOT NULL,
                fechaSalida DATE NOT NULL,
                CONSTRAINT reservacion_pk PRIMARY KEY (idReservacion, idUsuario, idHabitacion)
);


ALTER SEQUENCE reservacion_idreservacion_seq OWNED BY reservacion.idReservacion;

CREATE SEQUENCE inscripcion_idinscripcion_seq;

CREATE TABLE inscripcion (
                idInscripcion INTEGER NOT NULL DEFAULT nextval('inscripcion_idinscripcion_seq'),
                idEvento INTEGER NOT NULL,
                idUsuario INTEGER NOT NULL,
                claveAcceso INTEGER NOT NULL,
                canParticipantes INTEGER NOT NULL,
                codBanco VARCHAR NOT NULL,
                aprobada BOOLEAN NOT NULL,
                idDescuento INTEGER NOT NULL,
                CONSTRAINT inscripcion_pk PRIMARY KEY (idInscripcion, idEvento, idUsuario)
);


ALTER SEQUENCE inscripcion_idinscripcion_seq OWNED BY inscripcion.idInscripcion;

CREATE SEQUENCE pago_idpago_seq;

CREATE TABLE pago (
                idPago VARCHAR NOT NULL DEFAULT nextval('pago_idpago_seq'),
                monto DOUBLE PRECISION NOT NULL,
                idInscripcion INTEGER NOT NULL,
                idEvento INTEGER NOT NULL,
                idUsuario INTEGER NOT NULL,
                CONSTRAINT pago_pk PRIMARY KEY (idPago)
);


ALTER SEQUENCE pago_idpago_seq OWNED BY pago.idPago;

CREATE SEQUENCE participacion_idparticipacion_seq;

CREATE TABLE participacion (
                idParticipacion INTEGER NOT NULL DEFAULT nextval('participacion_idparticipacion_seq'),
                idUsuario INTEGER NOT NULL,
                idActividad INTEGER NOT NULL,
                canParticipantes INTEGER NOT NULL,
                CONSTRAINT participacion_pk PRIMARY KEY (idParticipacion, idUsuario, idActividad)
);


ALTER SEQUENCE participacion_idparticipacion_seq OWNED BY participacion.idParticipacion;

CREATE SEQUENCE participante_idparticipante_seq;

CREATE TABLE participante (
                idParticipante INTEGER NOT NULL DEFAULT nextval('participante_idparticipante_seq'),
                nombre VARCHAR NOT NULL,
                apellido1 VARCHAR NOT NULL,
                apellido2 VARCHAR NOT NULL,
                direccion VARCHAR NOT NULL,
                ciudad VARCHAR NOT NULL,
                pais VARCHAR NOT NULL,
                compania VARCHAR NOT NULL,
                telefono VARCHAR,
                folio VARCHAR NOT NULL,
                idParticipacion INTEGER NOT NULL,
                idUsuario INTEGER NOT NULL,
                idActividad INTEGER NOT NULL,
                CONSTRAINT participante_pk PRIMARY KEY (idParticipante)
);


ALTER SEQUENCE participante_idparticipante_seq OWNED BY participante.idParticipante;

ALTER TABLE hotel ADD CONSTRAINT evento_hotel_fk
FOREIGN KEY (idEvento)
REFERENCES evento (idEvento)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE actividad ADD CONSTRAINT evento_actividad_fk
FOREIGN KEY (idEvento)
REFERENCES evento (idEvento)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE paquete ADD CONSTRAINT evento_paquete_fk
FOREIGN KEY (idEvento)
REFERENCES evento (idEvento)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE inscripcion ADD CONSTRAINT evento_inscripcion_fk
FOREIGN KEY (idEvento)
REFERENCES evento (idEvento)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE cuotas ADD CONSTRAINT paquete_cuotas_fk
FOREIGN KEY (idPaquete)
REFERENCES paquete (idPaquete)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE descuento ADD CONSTRAINT paquete_descuento_fk
FOREIGN KEY (idPaquete)
REFERENCES paquete (idPaquete)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE inscripcion ADD CONSTRAINT descuento_inscripcion_fk
FOREIGN KEY (idDescuento)
REFERENCES descuento (idDescuento)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE tematica ADD CONSTRAINT actividad_tematica_fk
FOREIGN KEY (idActividad)
REFERENCES actividad (idActividad)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE participacion ADD CONSTRAINT actividad_participacion_fk
FOREIGN KEY (idActividad)
REFERENCES actividad (idActividad)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE habitacion ADD CONSTRAINT hotel_habitacion_fk
FOREIGN KEY (idHotel)
REFERENCES hotel (idHotel)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE reservacion ADD CONSTRAINT habitacion_reservacion_fk
FOREIGN KEY (idHabitacion)
REFERENCES habitacion (idHabitacion)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE usuario ADD CONSTRAINT cuenta_usuario_fk
FOREIGN KEY (idCuenta)
REFERENCES cuenta (idCuenta)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE participacion ADD CONSTRAINT usuario_participacion_fk
FOREIGN KEY (idUsuario)
REFERENCES usuario (idUsuario)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE inscripcion ADD CONSTRAINT usuario_inscripcion_fk
FOREIGN KEY (idUsuario)
REFERENCES usuario (idUsuario)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE reservacion ADD CONSTRAINT usuario_reservacion_fk
FOREIGN KEY (idUsuario)
REFERENCES usuario (idUsuario)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE pago ADD CONSTRAINT inscripcion_pago_fk
FOREIGN KEY (idInscripcion, idEvento, idUsuario)
REFERENCES inscripcion (idInscripcion, idEvento, idUsuario)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE participante ADD CONSTRAINT participacion_participante_fk
FOREIGN KEY (idParticipacion, idActividad, idUsuario)
REFERENCES participacion (idParticipacion, idActividad, idUsuario)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;