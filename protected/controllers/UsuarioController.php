<?php

class UsuarioController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
        
        //mis declaraciones, defino el evento que me llegara para buscar los hoteles
        public $evento;

        /**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
        
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','inscripcion','captcha','cargarhoteles','cargarhabitaciones'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                $model=new Usuario;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Usuario']))
		{
			$model->attributes=$_POST['Usuario'];                        
			if($model->save())
				$this->redirect(array('view','id'=>$model->idusuario));
		}

		$this->render('create',array(
			'model'=>$model,
		));                
	}
       
        public function actionInscripcion(){
          
                $model = new InscripcionForm;  
                 //seleccion del idevento pasado
                  /*  $this->evento=$_GET["evento"];
                    $sql="select idevento from evento where nombre = '".$this->evento."'".";";
                        $connection=Yii::app()->db;
                        $command=$connection->createCommand($sql);
                        $result=$command->query();                    
                        $b->idevento=$result["idevento"];*/
                        //$model->idevento = $b;
                        //echo $model->idevento;
                $model->idevento = '1';
    
                if(isset($_POST["InscripcionForm"])){    
                    echo 'pruebaaaa';
                    $model->attributes = $_POST["InscripcionForm"];
                    /*if($model->validate()){    // IMPORTANTE
                          if( false != $model->crearObjetos($model) ){ 
                             $this->redirect(array("oktodobien"));
                           } else{
                              $this->renderText($model->error_html);
                           }
                          return;
                    }*/                  
                    $model->crearObjetos();
                }               
                $this->render("inscripcion",array("model"=>$model));
        } 
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Usuario']))
		{
			$model->attributes=$_POST['Usuario'];                       
			if($model->save())
				$this->redirect(array('view','id'=>$model->idusuario,'operacion'=>'update'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Usuario');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Usuario('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Usuario']))
			$model->attributes=$_GET['Usuario'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Usuario the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Usuario::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Usuario $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='usuario-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        //Llenar el combo de hoteles
        public function actionCargarhoteles()
        {            
            $data=  Hotel::model()->findAllBySql(
            "select * from hotel where idevento
            =:keyword order by nombre",            
            array(':keyword'=>$_POST['InscripcionForm']['idevento']));
            
            $data=CHtml::listData($data,'idhotel','nombre');
            foreach($data as $value=>$name)
            {
                echo CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
            }           
        }
        
         //Llenar el combo de hoteles pasandole el nombre del hotel
        /*public function actionCargarhoteles()
        { 
             //seleccion del idevento pasado
                $this->evento=$_GET["evento"];
                $sql='select idevento from evento where nombre = '.$this->evento.';';
                    $connection=Yii::app()->db;
                    $command=$connection->createCommand($sql);
                    $row=$command->queryRow();                    
                    $b->idevento=$row["idevento"];
                    
            $data=  Hotel::model()->findAllBySql(
            "select * from hotel where idevento
            =:keyword order by nombre",            
            array(':keyword'=>$b));
            
            $data=CHtml::listData($data,'idhotel','nombre');
            foreach($data as $value=>$name)
            {
                echo CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
            }           
        }*/
        
        //Llenar el combo de habitaciones
         public function actionCargarhabitaciones()
        {            
            $data= Habitacion::model()->findAllBySql(
            "select * from habitacion where idhotel
            =:keyword order by numero",            
            array(':keyword'=>$_POST['InscripcionForm']['idhotel']));
            
            $data=CHtml::listData($data,'idhabitacion','numero');
            foreach($data as $value=>$nombre)
            {
                echo CHtml::tag('option', array('value'=>$value),CHtml::encode($nombre),true);
            }           
        }
}
