<?php

class UsuarioController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','inscripcion'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                $a = new Usuario();
                $b =new Cuenta();		

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                $this->performAjaxValidation(array($a,$b));
		if(isset($_POST['Usuario'],$_POST['Cuenta']))
		{
                    $a->attributes=$_POST['Usuario'];
                    $b->attributes=$_POST['Cuenta']; 
                    //campos por defecto de la cuenta
                    $b->fechaalta = date("Y-m-d");
                    $b->rol = 'usuario';
                    
                    $sql='select max(idcuenta) from cuenta;';
                    $connection=Yii::app()->db;
                    $command=$connection->createCommand($sql);
                    $row=$command->queryRow();
                    $row["max"]++;
                    $b->idcuenta=$row["max"];
                    $a->idcuenta=$row["max"];
                    $b->password=md5($b->password);
                    if($b->save() && $a->save())
                    $this->redirect(array('view','id'=>$a->idusuario));                    
		}
                $operacion = 'create';
		$this->render('create',array('a'=>$a,'b'=>$b,'operacion'=>$operacion));
                
	}
        public function crearObjetos($model){
                // solo llamarla si se ha pasado la validación.
               // echo 'submit ok';
               $tr = Yii::app()->db->beginTransaction();
               //$_cuenta = Cuenta::model()->findByAttributes(array("idcuenta"=>$this->$idcuenta));
               //if(null == $_cuenta){
                    $_cuenta = new Cuenta;
                    $_cuenta->username = $model->username;
                    $_cuenta->password = $model->password;
                    $_cuenta->fechaalta = $model->fechaalta;
                    $_cuenta->rol = $model->rol;
                    echo  $_cuenta->username;
                    echo  $_cuenta->fechaalta;
                    /*if(!$_cuenta->insert()){
                         $this->error_html = CHtml::errorSummary($_cuenta);
                         $tr->rollback();    // se cancela la transaccion
                         return false;        // porque el cliente no se pudo crear
                    }*/
               //}
       
               /*$_usuario = new Usuario;
               $_usuario->nombre = $this->nombre; 
               $_usuario->apellido1 = $this->apellido1;
               $_usuario->apellido2 = $this->apellido2;
               $_usuario->email = $this->email;
               $_usuario->direccion = $this->direccion;
               $_usuario->ciudad = $this->ciudad;
               $_usuario->pais = $this->pais;
               $_usuario->codigopostal = $this->codigopostal;
               $_usuario->telefono = $this->telefono;
               $_usuario->compania = $this->compania;
               $_usuario->idcuenta = $_cuenta->idcuenta;
*/
               /*if( false == $_usuario->insert()){
                    $this->error_html = CHtml::errorSummary($_usuario);
                    $tr->rollback();
                    return false;
               }
               $tr->commit();
              return $_usuario->primarykey;*/
         }
        public function actionInscripcion(){
          
                $model = new InscripcionForm;
                if(isset($_POST["InscripcionForm"])){    
                    echo 'pruebaaaa';
                    $model->attributes = $_POST["InscripcionForm"];
                    /*if($model->validate()){    // IMPORTANTE
                          if( false != $model->crearObjetos($model) ){ 
                             $this->redirect(array("oktodobien"));
                           } else{
                              $this->renderText($model->error_html);
                           }
                          return;
                    }*/
                 
                    $model->crearObjetos();
                }
                // echo "prueba";
                $this->render("inscripcion",array("model"=>$model));
        } 
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Usuario']))
		{
			$model->attributes=$_POST['Usuario'];
                        $a = $model->idcuenta;
			if($model->save())
				$this->redirect(array('view','id'=>$model->idusuario,'operacion'=>'update'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Usuario');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Usuario('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Usuario']))
			$model->attributes=$_GET['Usuario'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Usuario the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Usuario::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Usuario $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='usuario-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
