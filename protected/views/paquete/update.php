<?php
/* @var $this PaqueteController */
/* @var $model Paquete */

$this->breadcrumbs=array(
	'Paquetes'=>array('index'),
	$model->idpaquete=>array('view','id'=>$model->idpaquete),
	'Update',
);

$this->menu=array(
	array('label'=>'List Paquete', 'url'=>array('index')),
	array('label'=>'Create Paquete', 'url'=>array('create')),
	array('label'=>'View Paquete', 'url'=>array('view', 'id'=>$model->idpaquete)),
	array('label'=>'Manage Paquete', 'url'=>array('admin')),
);
?>

<h1>Update Paquete <?php echo $model->idpaquete; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>