<?php
/* @var $this PaqueteController */
/* @var $model Paquete */

$this->breadcrumbs=array(
	'Paquetes'=>array('index'),
	$model->idpaquete,
);

$this->menu=array(
	array('label'=>'List Paquete', 'url'=>array('index')),
	array('label'=>'Create Paquete', 'url'=>array('create')),
	array('label'=>'Update Paquete', 'url'=>array('update', 'id'=>$model->idpaquete)),
	array('label'=>'Delete Paquete', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idpaquete),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Paquete', 'url'=>array('admin')),
);
?>

<h1>View Paquete #<?php echo $model->idpaquete; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idpaquete',
		'nombre',
		'descripcion',
		'idevento',
	),
)); ?>
