<?php
/* @var $this PaqueteController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Paquetes',
);

$this->menu=array(
	array('label'=>'Create Paquete', 'url'=>array('create')),
	array('label'=>'Manage Paquete', 'url'=>array('admin')),
);
?>

<h1>Paquetes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
