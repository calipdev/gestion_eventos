<?php
/* @var $this UsuarioController */
/* @var $model Usuario */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'usuario-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>
<?php
	if ($a->isNewRecord==false) { 
            if($operacion != 'update'){
                $b=Cuenta::model()->findByPk($a->idcuenta); 
                echo $form->errorSummary(array($a,$b));}
            else
               echo $form->errorSummary(array($a));} ?>
        
        <div class="row">
            <?php if ($operacion  == 'create') {
                echo $form->labelEx($b, 'username');           
                echo $form->textField($b,'username',array('size'=>20,'maxlength'=>20));
		echo $form->error($b,'username');}?>
	</div>

	<div class="row">
            <?php if($operacion == 'create'){
		echo $form->labelEx($b,'password'); 
		echo $form->passwordField($b,'password'); 
		echo $form->error($b,'password');}?>
	</div>	
	

	<div class="row">
		<?php echo $form->labelEx($a,'nombre'); ?>
		<?php echo $form->textField($a,'nombre',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($a,'nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($a,'apellido1'); ?>
		<?php echo $form->textField($a,'apellido1',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($a,'apellido1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($a,'apellido2'); ?>
		<?php echo $form->textField($a,'apellido2',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($a,'apellido2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($a,'email'); ?>
		<?php echo $form->textField($a,'email'); ?>
		<?php echo $form->error($a,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($a,'direccion'); ?>
		<?php echo $form->textField($a,'direccion'); ?>
		<?php echo $form->error($a,'direccion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($a,'ciudad'); ?>
		<?php echo $form->textField($a,'ciudad'); ?>
		<?php echo $form->error($a,'ciudad'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($a,'pais'); ?>
		<?php echo $form->textField($a,'pais'); ?>
		<?php echo $form->error($a,'pais'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($a,'codigopostal'); ?>
		<?php echo $form->textField($a,'codigopostal'); ?>
		<?php echo $form->error($a,'codigopostal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($a,'telefono'); ?>
		<?php echo $form->textField($a,'telefono'); ?>
		<?php echo $form->error($a,'telefono'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($a,'compania'); ?>
		<?php echo $form->textField($a,'compania'); ?>
		<?php echo $form->error($a,'compania'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($a->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->