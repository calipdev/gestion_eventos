<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->

	<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Home', 'url'=>array('/site/index')),
				array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
				array('label'=>'Contact', 'url'=>array('/site/contact')),
                                array('label'=>'Eventos', 'url'=>array('/evento/')),                            
                                array('label'=>'Hoteles', 'url'=>array('/hotel/')),
                                array('label'=>'Habitaciones', 'url'=>array('/habitacion/')),
                                array('label'=>'Paquete', 'url'=>array('/paquete/')),
                                array('label'=>'Cuota', 'url'=>array('/cuota/')),
                                array('label'=>'Descuento', 'url'=>array('/descuento/')),
                                array('label'=>'Actividad', 'url'=>array('/actividad/')),
                                array('label'=>'Tematica', 'url'=>array('/tematica/')),
                                array('label'=>'Usuarios', 'url'=>array('/usuario/')),
                                array('label'=>'Inscripcion', 'url'=>array('/usuario/inscripcion')),
                                array('label'=>'Cuentas', 'url'=>array('/cuenta/')),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); ?>
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>
        <?php
        echo CHtml::link('Evento1', array('Usuario/inscripcion', 'evento'=>'Salud2014'));
        ?>
        
        <?php
        echo CHtml::ajaxLink(
                'pruebaAjax1',
                array('Site/ejemploajax1'),
                array('update'=>'#divajax1')
                );
        ?>
        <div id="divajax1"></div>
        <?php
        echo CHtml::ajaxLink(
                'pruebaAjax2',
                array('Site/ejemploajax2'),
                array(
                    'update'=>'#divajax2',
                    'beforeSend'=> "function()
                        {
                            $('#divajax2').hide('fast');
                            $('#divajaxcargando').show('fast');
                        }",
                    'complete'=> "function()
                        {
                            $('#divajaxcargando').hide('fast');
                            $('#divajax2').show('fast');
                        }",
                    'error'=> "function()
                        {
                            $('#divajaxcargando').hide('fast');
                           // $('#divajaxcargando').html('Ocurrio un error en el envio');
                            $('#divajax2').hide('fast');
                            alert('Error al procesar la peticion');
                        }",
                    /*'success'=> "function()
                        {                            
                           alert('Se ha procesado correctamente la peticion');
                           // $('#divajax2').show('fast');
                        }"*/
                    )
                );
        ?>
        <div id="divajaxcargando" style="display: none" > Cargando espere por favor...</div>
        <div id="divajax2"></div>
        
        
	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
