<?php
/* @var $this HabitacionController */
/* @var $data Habitacion */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idhabitacion')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idhabitacion), array('view', 'id'=>$data->idhabitacion)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numero')); ?>:</b>
	<?php echo CHtml::encode($data->numero); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo')); ?>:</b>
	<?php echo CHtml::encode($data->tipo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('costo')); ?>:</b>
	<?php echo CHtml::encode($data->costo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('maxpersonas')); ?>:</b>
	<?php echo CHtml::encode($data->maxpersonas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ocupada')); ?>:</b>
	<?php echo CHtml::encode($data->ocupada); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idhotel')); ?>:</b>
	<?php echo CHtml::encode($data->idhotel); ?>
	<br />


</div>