<?php
/* @var $this HabitacionController */
/* @var $model Habitacion */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idhabitacion'); ?>
		<?php echo $form->textField($model,'idhabitacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'numero'); ?>
		<?php echo $form->textField($model,'numero'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tipo'); ?>
		<?php echo $form->textField($model,'tipo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'costo'); ?>
		<?php echo $form->textField($model,'costo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'maxpersonas'); ?>
		<?php echo $form->textField($model,'maxpersonas'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ocupada'); ?>
		<?php echo $form->checkBox($model,'ocupada'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idhotel'); ?>
		<?php echo $form->textField($model,'idhotel'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->