<?php
/* @var $this HabitacionController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Habitacions',
);

$this->menu=array(
	array('label'=>'Create Habitacion', 'url'=>array('create')),
	array('label'=>'Manage Habitacion', 'url'=>array('admin')),
);
?>

<h1>Habitacions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
