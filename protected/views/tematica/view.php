<?php
/* @var $this TematicaController */
/* @var $model Tematica */

$this->breadcrumbs=array(
	'Tematicas'=>array('index'),
	$model->idtematica,
);

$this->menu=array(
	array('label'=>'List Tematica', 'url'=>array('index')),
	array('label'=>'Create Tematica', 'url'=>array('create')),
	array('label'=>'Update Tematica', 'url'=>array('update', 'id'=>$model->idtematica)),
	array('label'=>'Delete Tematica', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idtematica),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Tematica', 'url'=>array('admin')),
);
?>

<h1>View Tematica #<?php echo $model->idtematica; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idtematica',
		'nombre',
		'descripcion',
		'idactividad',
	),
)); ?>
