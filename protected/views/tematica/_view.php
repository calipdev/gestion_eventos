<?php
/* @var $this TematicaController */
/* @var $data Tematica */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idtematica')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idtematica), array('view', 'id'=>$data->idtematica)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idactividad')); ?>:</b>
	<?php echo CHtml::encode($data->idactividad); ?>
	<br />


</div>