<?php
/* @var $this TematicaController */
/* @var $model Tematica */

$this->breadcrumbs=array(
	'Tematicas'=>array('index'),
	$model->idtematica=>array('view','id'=>$model->idtematica),
	'Update',
);

$this->menu=array(
	array('label'=>'List Tematica', 'url'=>array('index')),
	array('label'=>'Create Tematica', 'url'=>array('create')),
	array('label'=>'View Tematica', 'url'=>array('view', 'id'=>$model->idtematica)),
	array('label'=>'Manage Tematica', 'url'=>array('admin')),
);
?>

<h1>Update Tematica <?php echo $model->idtematica; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>