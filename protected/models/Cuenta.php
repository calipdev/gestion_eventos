<?php

/**
 * This is the model class for table "public.cuenta".
 *
 * The followings are the available columns in table 'public.cuenta':
 * @property integer $idcuenta
 * @property string $username
 * @property string $password
 * @property string $fechaalta
 * @property string $rol
 *
 * The followings are the available model relations:
 * @property Usuario[] $usuarios
 * @property Reservacion[] $reservacion
 */
class Cuenta extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'public.cuenta';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, password', 'required'),
			array('username', 'length', 'max'=>20),
                        array('username', 'unique'),
			array('fechaalta, rol', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idcuenta, username, password, fechaalta, rol', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuarios' => array(self::HAS_MANY, 'Usuario', 'idcuenta'),			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idcuenta' => 'Idcuenta',
			'username' => 'Username',
			'password' => 'Password',
			'fechaalta' => 'Fechaalta',
			'rol' => 'Rol',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idcuenta',$this->idcuenta);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('fechaalta',$this->fechaalta,true);
		$criteria->compare('rol',$this->rol,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Cuenta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        
        public function validatePassword($password)
        {
            return $this->hashPassword($password)===$this->password;
        }
        
        public function hashPassword($password)
        {
            return md5($password);
        }
        
       /* public function behaviors()
        {
            return array(
                'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'fechaalta',
                //'updateAttribute' => 'modified_date',
                'setUpdateOnCreate' => true,
                ),
                'BlameableBehavior' => array(
                'class' => 'application.components.BlameableBehavior',
                'createdByColumn' => 'fechaalta',
                'updatedByColumn' => 'modified_by',
                ),
            );
        }*/
}
