<?php

/**
 * This is the model class for table "public.evento".
 *
 * The followings are the available columns in table 'public.evento':
 * @property integer $idevento
 * @property string $nombre
 * @property string $tipo
 * @property string $fechainicio
 * @property string $fechafin
 * @property double $costo
 * @property string $tipoventa
 * @property string $direccion
 * @property string $ciudad
 * @property string $pais
 *
 * The followings are the available model relations:
 * @property Inscripcion[] $inscripcions
 * @property Hotel[] $hoteles
 * @property Actividad[] $actividades
 * @property Paquete[] $paquetes
 */
class Evento extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'public.evento';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, tipo, fechainicio, fechafin, costo, tipoventa, direccion, ciudad, pais', 'required'),
			array('costo', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idevento, nombre, tipo, fechainicio, fechafin, costo, tipoventa, direccion, ciudad, pais', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'inscripciones' => array(self::HAS_MANY, 'Inscripcion', 'idevento'),
			'hoteles' => array(self::HAS_MANY, 'Hotel', 'idevento'),
			'actividades' => array(self::HAS_MANY, 'Actividad', 'idevento'),
			'paquetes' => array(self::HAS_MANY, 'Paquete', 'idevento'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idevento' => 'Idevento',
			'nombre' => 'Nombre',
			'tipo' => 'Tipo',
			'fechainicio' => 'Fechainicio',
			'fechafin' => 'Fechafin',
			'costo' => 'Costo',
			'tipoventa' => 'Tipoventa',
			'direccion' => 'Direccion',
			'ciudad' => 'Ciudad',
			'pais' => 'Pais',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idevento',$this->idevento);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('tipo',$this->tipo,true);
		$criteria->compare('fechainicio',$this->fechainicio,true);
		$criteria->compare('fechafin',$this->fechafin,true);
		$criteria->compare('costo',$this->costo);
		$criteria->compare('tipoventa',$this->tipoventa,true);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('ciudad',$this->ciudad,true);
		$criteria->compare('pais',$this->pais,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Evento the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
