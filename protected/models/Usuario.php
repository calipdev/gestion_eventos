<?php

/**
 * This is the model class for table "public.usuario".
 *
 * The followings are the available columns in table 'public.usuario':
 * @property integer $idusuario
 * @property string $nombre
 * @property string $apellido1
 * @property string $apellido2
 * @property string $email
 * @property string $direccion
 * @property string $ciudad
 * @property string $pais
 * @property string $codigopostal
 * @property string $telefono
 * @property string $compania
 * @property integer $idcuenta
 *
 * The followings are the available model relations:
 * @property Participacion[] $participaciones
 * @property Inscripcion[] $inscripciones
 * @property Reservacion[] $reservaciones
 * @property Cuenta $idcuenta
 */
class Usuario extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'public.usuario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, apellido1, apellido2, email, direccion, ciudad, pais, codigopostal, telefono, compania, idcuenta', 'required'),
			array('idcuenta', 'numerical', 'integerOnly'=>true),
			array('nombre, apellido1, apellido2', 'length', 'max'=>40),
                        array('email, idcuenta','unique'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idusuario, nombre, apellido1, apellido2, email, direccion, ciudad, pais, codigopostal, telefono, compania, idcuenta', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'participaciones' => array(self::HAS_MANY, 'Participacion', 'idusuario'),
			'inscripciones' => array(self::HAS_MANY, 'Inscripcion', 'idusuario'),
			'reservaciones' => array(self::HAS_MANY, 'Reservacion', 'idusuario'),
			'idcuenta' => array(self::BELONGS_TO, 'Cuenta', 'idcuenta'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idusuario' => 'Idusuario',
			'nombre' => 'Nombre',
			'apellido1' => 'Apellido1',
			'apellido2' => 'Apellido2',
			'email' => 'Email',
			'direccion' => 'Direccion',
			'ciudad' => 'Ciudad',
			'pais' => 'Pais',
			'codigopostal' => 'Codigopostal',
			'telefono' => 'Telefono',
			'compania' => 'Compania',
			'idcuenta' => 'Idcuenta',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idusuario',$this->idusuario);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('apellido1',$this->apellido1,true);
		$criteria->compare('apellido2',$this->apellido2,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('ciudad',$this->ciudad,true);
		$criteria->compare('pais',$this->pais,true);
		$criteria->compare('codigopostal',$this->codigopostal,true);
		$criteria->compare('telefono',$this->telefono,true);
		$criteria->compare('compania',$this->compania,true);
		$criteria->compare('idcuenta',$this->idcuenta);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Usuario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
