<?php

/**
 * This is the model class for table "public.descuento".
 *
 * The followings are the available columns in table 'public.descuento':
 * @property integer $iddescuento
 * @property double $monto
 * @property string $descripcion
 * @property integer $porcentage
 * @property integer $idpaquete
 *
 * The followings are the available model relations:
 * @property Inscripcion[] $inscripcions
 * @property Paquete $idpaquete
 */
class Descuento extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'public.descuento';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('monto, descripcion, porcentage, idpaquete', 'required'),
			array('porcentage, idpaquete', 'numerical', 'integerOnly'=>true),
			array('monto', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('iddescuento, monto, descripcion, porcentage, idpaquete', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'inscripciones' => array(self::HAS_MANY, 'Inscripcion', 'iddescuento'),
			'idpaquete' => array(self::BELONGS_TO, 'Paquete', 'idpaquete'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'iddescuento' => 'Iddescuento',
			'monto' => 'Monto',
			'descripcion' => 'Descripcion',
			'porcentage' => 'Porcentage',
			'idpaquete' => 'Idpaquete',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('iddescuento',$this->iddescuento);
		$criteria->compare('monto',$this->monto);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('porcentage',$this->porcentage);
		$criteria->compare('idpaquete',$this->idpaquete);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Descuento the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
