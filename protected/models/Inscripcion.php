<?php

/**
 * This is the model class for table "public.inscripcion".
 *
 * The followings are the available columns in table 'public.inscripcion':
 * @property integer $idinscripcion
 * @property integer $idevento
 * @property integer $idusuario
 * @property integer $claveacceso
 * @property integer $canparticipantes
 * @property string $codbanco
 * @property boolean $aprobada
 * @property integer $iddescuento
 *
 * The followings are the available model relations:
 * @property Pago[] $pagos
 * @property Pago[] $pagos1
 * @property Pago[] $pagos2
 * @property Evento $idevento
 * @property Descuento $iddescuento
 * @property Usuario $idusuario
 */
class Inscripcion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'public.inscripcion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idevento, idusuario, canparticipantes, codbanco', 'required'),
			array('idevento, idusuario, claveacceso, canparticipantes, iddescuento', 'numerical', 'integerOnly'=>true),
			array('aprobada', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idinscripcion, idevento, idusuario, claveacceso, canparticipantes, codbanco, aprobada, iddescuento', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pagos' => array(self::HAS_MANY, 'Pago', 'idinscripcion'),
			'pagos1' => array(self::HAS_MANY, 'Pago', 'idevento'),
			'pagos2' => array(self::HAS_MANY, 'Pago', 'idusuario'),
			'idevento' => array(self::BELONGS_TO, 'Evento', 'idevento'),
			'iddescuento' => array(self::BELONGS_TO, 'Descuento', 'iddescuento'),
			'idusuario' => array(self::BELONGS_TO, 'Usuario', 'idusuario'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idinscripcion' => 'Idinscripcion',
			'idevento' => 'Idevento',
			'idusuario' => 'Idusuario',
			'claveacceso' => 'Claveacceso',
			'canparticipantes' => 'Canparticipantes',
			'codbanco' => 'Codbanco',
			'aprobada' => 'Aprobada',
			'iddescuento' => 'Iddescuento',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idinscripcion',$this->idinscripcion);
		$criteria->compare('idevento',$this->idevento);
		$criteria->compare('idusuario',$this->idusuario);
		$criteria->compare('claveacceso',$this->claveacceso);
		$criteria->compare('canparticipantes',$this->canparticipantes);
		$criteria->compare('codbanco',$this->codbanco,true);
		$criteria->compare('aprobada',$this->aprobada);
		$criteria->compare('iddescuento',$this->iddescuento);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Inscripcion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
