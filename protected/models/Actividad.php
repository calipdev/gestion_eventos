<?php

/**
 * This is the model class for table "public.actividad".
 *
 * The followings are the available columns in table 'public.actividad':
 * @property integer $idactividad
 * @property string $nombre
 * @property string $ciudad
 * @property string $lugar
 * @property string $hora
 * @property string $duracion
 * @property string $descripcion
 * @property integer $maxparticipantes
 * @property boolean $ocupada
 * @property integer $idevento
 *
 * The followings are the available model relations:
 * @property Participacion[] $participacions
 * @property Evento $idevento
 * @property Tematica[] $tematicas
 */
class Actividad extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'public.actividad';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, ciudad, lugar, hora, duracion, descripcion, maxparticipantes, ocupada, idevento', 'required'),
			array('maxparticipantes, idevento', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idactividad, nombre, ciudad, lugar, hora, duracion, descripcion, maxparticipantes, ocupada, idevento', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'participaciones' => array(self::HAS_MANY, 'Participacion', 'idactividad'),
			'idevento' => array(self::BELONGS_TO, 'Evento', 'idevento'),
			'tematicas' => array(self::HAS_MANY, 'Tematica', 'idactividad'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idactividad' => 'Idactividad',
			'nombre' => 'Nombre',
			'ciudad' => 'Ciudad',
			'lugar' => 'Lugar',
			'hora' => 'Hora',
			'duracion' => 'Duracion',
			'descripcion' => 'Descripcion',
			'maxparticipantes' => 'Maxparticipantes',
			'ocupada' => 'Ocupada',
			'idevento' => 'Idevento',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idactividad',$this->idactividad);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('ciudad',$this->ciudad,true);
		$criteria->compare('lugar',$this->lugar,true);
		$criteria->compare('hora',$this->hora,true);
		$criteria->compare('duracion',$this->duracion,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('maxparticipantes',$this->maxparticipantes);
		$criteria->compare('ocupada',$this->ocupada);
		$criteria->compare('idevento',$this->idevento);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Actividad the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
