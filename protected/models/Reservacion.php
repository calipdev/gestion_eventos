<?php

/**
 * This is the model class for table "public.reservacion".
 *
 * The followings are the available columns in table 'public.reservacion':
 * @property integer $idreservacion
 * @property integer $idusuario
 * @property integer $idhabitacion
 * @property string $fechaentrada
 * @property string $fechasalida
 *
 * The followings are the available model relations:
 * @property Habitacion $idhabitacion
 * @property Usuario $idusuario
 */
class Reservacion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'public.reservacion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idusuario, idhabitacion, fechaentrada, fechasalida', 'required'),
			array('idusuario, idhabitacion', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idreservacion, idusuario, idhabitacion, fechaentrada, fechasalida', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idhabitacion' => array(self::BELONGS_TO, 'Habitacion', 'idhabitacion'),
			'idusuario' => array(self::BELONGS_TO, 'Usuario', 'idusuario'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idreservacion' => 'Idreservacion',
			'idusuario' => 'Idusuario',
			'idhabitacion' => 'Idhabitacion',
			'fechaentrada' => 'Fechaentrada',
			'fechasalida' => 'Fechasalida',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idreservacion',$this->idreservacion);
		$criteria->compare('idusuario',$this->idusuario);
		$criteria->compare('idhabitacion',$this->idhabitacion);
		$criteria->compare('fechaentrada',$this->fechaentrada,true);
		$criteria->compare('fechasalida',$this->fechasalida,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Reservacion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
